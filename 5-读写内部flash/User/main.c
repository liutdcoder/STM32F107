/**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   串口中断接收测试
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火 F103-霸道 STM32 开发板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */ 
 
 
#include "stm32f10x.h"
#include "bsp_usart.h"
#include "rw_flash.h"
/**
  * @brief  主函数
  * @param  无
  * @retval 无
  */
#define BUF_LEN      3500
uint8_t buf[BUF_LEN] = {0};
uint32_t buf1[BUF_LEN] = { 0};
uint8_t aa[BUF_LEN] = { 0};
uint32_t bb[BUF_LEN] = { 0};
uint32_t pbuf[512] = {0};
uint32_t a = 0;
uint32_t rev = 0;
void iap_write_appbin(u32 appxaddr,u8 *appbuf,u32 appsize);
void IAP_Write_Bin(uint8_t *Read_buf,uint32_t Write_addr,uint32_t NumToWrite);
int byte_trsnsf(uint8_t *aa,uint32_t *bb,uint16_t size);
int main(void)
{	
  /*初始化USART 配置模式为 115200 8-N-1，中断接收*/
  USART_Config();	
	/* 发送一个字符串 */
	Usart_SendString( DEBUG_USARTx,"这是一个串口中断接收回显实验\n");
	printf("欢迎使用野火STM32开发板\n\n\n\n");
	uint16_t i = 0;
	uint16_t j = 0;
	for(j = 0 ; j < 800;j++)
		for(i = 0; i < 4; i++)
		{
			buf[i +j *4] = i+1;
			printf("buf[%d] = %d\r\n",i+j*4,i+1);
		}
		IAP_Write_Bin(buf,SEC,562);
//	for( i = 0 ;i< 2048;i+=4)
//		{
//			a = buf[i];
//			a += buf[i+1]<<8;
//			a += buf[i+2]<<16;
//			a += buf[i+3]<<24;
//			pbuf[i/4] = a;
//			printf("a = %x\r\n",a);
//		}
//	if(PASSED == Erase_Write_Flash(SEC,(uint32_t*)pbuf,512))
//	{
//		printf("擦写成功\n");
//	}
//	else
//	{
//		printf("擦写失败\n");
//	}
	Internal_Flash_Read(SEC,(uint32_t *)buf1,512);
	for(i = 0; i <141; i++)
	{
		printf("data = %x\r\n",buf1[i]);
	}
  while(1)
	{	

	}	
}


void IAP_Write_Bin(uint8_t *Read_buf,uint32_t Write_addr,uint32_t NumToWrite)
{
	uint32_t t;
	uint16_t i=0;
	uint32_t temp;
	uint32_t fwaddr=Write_addr;//当前写入的地址
	u8 *dfu=Read_buf;
	uint32_t iapbuf[512] = {0};
	for(t=0;t<NumToWrite;t+=4)
	{						   
		temp=(u32)dfu[3]<<24;   
		temp|=(u32)dfu[2]<<16;    
		temp|=(u32)dfu[1]<<8;
		temp|=(u32)dfu[0];	  
		dfu+=4;//偏移4个字节
		iapbuf[i++]=temp;	    
		if(i==512)
		{
			i=0; 
			Erase_Write_Flash(fwaddr,iapbuf,512);
			fwaddr+=2048;//偏移2048  512*4=2048
		}
	} 
	if(i)
		Erase_Write_Flash(fwaddr,iapbuf,i);//将最后的一些内容字节写进去.
}


u32 iapbuf[512]; 	//2K字节缓存  
//appxaddr:应用程序的起始地址
//appbuf:应用程序CODE.
//appsize:应用程序大小(字节).
void iap_write_appbin(u32 appxaddr,u8 *appbuf,u32 appsize)
{
	u32 t;
	u16 i=0;
	u32 temp;
	u32 fwaddr=appxaddr;//当前写入的地址
	u8 *dfu=appbuf;
	for(t=0;t<appsize;t+=4)
	{						   
		temp=(u32)dfu[3]<<24;   
		temp|=(u32)dfu[2]<<16;    
		temp|=(u32)dfu[1]<<8;
		temp|=(u32)dfu[0];	  
		dfu+=4;//偏移4个字节
		iapbuf[i++]=temp;	    
		if(i==512)
		{
			i=0; 
			Erase_Write_Flash(fwaddr,iapbuf,512);
			fwaddr+=2048;//偏移2048  512*4=2048
		}
	} 
	if(i)
		Erase_Write_Flash(fwaddr,iapbuf,i);//将最后的一些内容字节写进去.  
}
	/*********************************************END OF FILE**********************/
