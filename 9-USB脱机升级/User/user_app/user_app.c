#include "user_app.h"
/******************************************************
* Brief     : 计算传输数据校验和
* Parameter : Data_buf;接受到的一帧数据
*							Data_Size:数据总长度（不带校验）
* Return    : 校验值
*******************************************************/
uint8_t Checksum_Check(uint8_t *Data_buf,uint8_t Data_Size)
{
	uint8_t i;
	uint8_t temp = 0;
	for( i = 1 ; i  < Data_Size; i++ )
	{
		temp += Data_buf[i]; 
	}
	/*type段和数据段相加 和 0xff 异或极为校验值*/
	return (temp^0xff);
}
/******************************************************
* Brief     : 软件复位
* Parameter : None
* Return    : None	
*******************************************************/
void SoftReset(void)
{
    __set_FAULTMASK(1); // 关闭所有中断
    NVIC_SystemReset(); // 复位
}
/******************************************************
* Brief     : 看门狗复位
* Parameter : None
* Return    : None	
*******************************************************/
void fWDG_init(void)
{
	if(RCC_GetFlagStatus(RCC_FLAG_IWDGRST) != RESET)				
	{
		printf("main : wdg reset\r\n");
		RCC_ClearFlag();											
	}
	
	RCC_LSICmd(ENABLE);											
	while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY)==RESET);				
	IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);					
	IWDG_SetPrescaler(IWDG_Prescaler_32);							
	IWDG_SetReload(2600);										
	IWDG_ReloadCounter();
	IWDG_Enable();
}

FRESULT result;												/*文件操作结果*/
FATFS fs;
FIL file;															/*文件指针*/
DIR  dir;															/*目录指针*/
FILINFO fno;													/*读取文件信息指针*/
UINT fnum;            					  		/* 文件成功读写数量 */
BYTE ReadBuffer[2048]={0};        		/* 读缓冲区，一次读取一个扇区 2K字节 */
uint32_t app_buf[512] = {0};

/******************************************************
* Brief     : 更新APP，调用此函数即可将指定bin文件写入指定地址
*	首先先读取要写入文件的信息，获取文件大小，然后根据文件大小，
* 计算所要擦除的文件大小，以扇区为单位进行数据擦除和写入，对于不足
*	一个扇区的，也对扇区进行擦除和写入，然后执行APP跳转。
* Parameter : File_Path:要写入的bin文件的路径
* Return    : Status
*					 0:无可用更新
*					 1:文件系统挂载失败
*					 2:文件打开失败
*					 3:文件读取失败
*					 4:文件信息读取失败（文件不存在）
*					 5:文件过大，无法写入
*					 6:文件写入flash失败
*******************************************************/

uint8_t APP_Updata(char * File_Path)
{	
	
	uint32_t j = 0;
	uint16_t buffer_count = 0;					//计算所需扇区数
	uint16_t buffer_rest = 0;						//最后一扇区的字节数
	uint8_t Status = 0;									//查看读写状态
	
	result = f_mount(&fs,"0:",1);	/* Mount a logical drive */
	if(result != FR_OK)
	{
		printf("\r\n文件系统挂载失败！\r\n");
		return 0x01;
   }
	result = f_stat(File_Path, &fno);

	/*输出文件信息，打印文件大小，文件改动时间（不准）*/
	if(result != FR_OK)
	{
			return 0x02;
	}
	printf("Size: %ld\n", fno.fsize);
        printf("Timestamp: %u/%02u/%02u, %02u:%02u\n",
               (fno.fdate >> 9) + 1980, fno.fdate >> 5 & 15, fno.fdate & 31,
               fno.ftime >> 11, fno.ftime >> 5 & 63);
	
	buffer_count = fno.fsize /FLASH_PAGE_SIZE ;						//扇区数
	buffer_rest = fno.fsize%FLASH_PAGE_SIZE;								//不足一个扇区剩下的字节数目
	
	/*打开bin文件*/
	result = f_open(&file, File_Path, FA_OPEN_EXISTING | FA_READ); 	
	/*打开成功*/
	if(result != FR_OK)
	{	
		return 0x03;
	}
		/*先将完整的扇区给写入*/
		for( j = 0 ;j < buffer_count ; j++)
		{	
			result = f_read(&file, ReadBuffer,sizeof(ReadBuffer), &fnum); 
			if(result != FR_OK)
			{
				return 0x04;
			}
	
			/*将数据写入到flash当中*/
			Status = IAP_Write_Appbin(ReadBuffer,SEC+j * FLASH_PAGE_SIZE,2048);
			if( Status != 0)
			{
				return 0x06;
			}
		}		
			/*读取剩下字节*/		
			result = f_read(&file, ReadBuffer,buffer_rest, &fnum); 
			if(result != FR_OK)
			{
				return 0x04;
			}
			/*将剩余的字节写入到flash中*/
			Status = IAP_Write_Appbin(ReadBuffer,SEC+buffer_count* FLASH_PAGE_SIZE,buffer_rest);
			if( Status != 0)
			{
				return 0x06;
			}
	/* 不再读写，关闭文件 */
	f_close(&file);
			
	/*解除挂载USB*/		
	result = f_mount(&fs,"0:",1);	/* Mount a logical drive */
			
	if(result != FR_OK)
  {
   return 0x01;
  }

	/*跳转到APP程序*/
//	iap_load_app(SEC);
	return 0;
}


/******************************************************
* Brief     : 检测是否存在td_force.bin
* Parameter :	None
* Return    : Status
*					 0:无可用更新
*					 1:文件系统挂载失败
*					 2:文件信息读取失败
*					 3:文件打开失败
*					 4:文件读取失败
*					 5:文件比对不一致，进入升级程序
*					 6:文件过大，无法升级
*******************************************************/
uint32_t USB_Buffer[512];
uint8_t Check_USB_Bin(char *file_path)
{
	uint8_t i = 0;
	uint16_t j = 0;
	uint8_t result = 0;
	
	uint8_t buffer_count = 0;				//满扇区个数
	uint16_t buffer_rest = 0;				//不满一个扇区的字节数
	
	result = f_mount(&fs,"0:",1);	/* Mount a logical drive */
	if(result == FR_OK)
	{
		printf("文件系统挂载成功\r\n");
		result = f_stat(file_path, &fno);
		
		if(result != FR_OK)
		{
			return 0x02;
		}
		
		buffer_count = fno.fsize/FLASH_PAGE_SIZE;
		buffer_rest  = fno.fsize%FLASH_PAGE_SIZE;
		
		printf("fno.fsize = %ld\r\n",fno.fsize);
		if(fno.fsize >= MAX_STORAGE)
		{
			result = 0x05;
		}
		
		result = f_open(&file,file_path,FA_OPEN_EXISTING | FA_READ);
		if(result != FR_OK)
		{
			return 0x03;
		}
		/*分扇区把数据比对一下*/
		for(i = 0; i < buffer_count ; i++)
		{
			result = f_read(&file, USB_Buffer,sizeof(USB_Buffer), &fnum);
			if(result != FR_OK)
			{
				return 0x05;
			}
			for(j = 0;j < 512; j++)
			{
				if(USB_Buffer[j] != *((uint32_t *)(SEC+ i*FLASH_PAGE_SIZE+j*4)))
				{
					return 0x05;
				}
			}
		}	
		/*把剩下不足一个扇区的数据比对一下*/
		result = f_read(&file, USB_Buffer,buffer_rest, &fnum);
		if(result != FR_OK)
		{
			return 0x04;
		}
		for(j = 0;j < (buffer_rest/4); j++)
		{ 
			if(USB_Buffer[j] != *((uint32_t *)(SEC+ FLASH_PAGE_SIZE*buffer_count+j*4)))
			{
				return 0x05;
			}
		}
	}
	else
	{
		printf("文件系统挂载失败\r\n");
		return 1;
	}
	return 0;
}
/******************************************************
* Brief     : 将数据封装成数据帧发送出去
*	数据格式		byte0:固定0x2e,byte1:数据类型,byte2:数据长度
*	byte3-byte6:数据固定为12345678，byte7:命令;
*	byte6-byten:数据,byten+1:校验。
* Parameter :	Data_buf:要发送的数据
*							Data_Length:发送的数据长度
*							Cmd_Type:命令类型
*							MUX_option:复用选项
*									1:开始升级
*									2:升级完成
*									3:升级失败
*									4:升级文件缺失
*									5:文件大小越界				
* Return    : Status		
*******************************************************/
void MCU_To_Host(uint8_t Cmd_Type,uint8_t MUX_option)
{
	uint8_t i = 0;
	uint8_t j=0;	
	uint8_t data_length = 0;
	uint8_t frame[22] = {0x2e,0x91,0x00,0x12,0x34,0x56,0x78,0x00};
				//	数据长度，包含了数据长度字节之后除校验的字节数
	frame[7] = Cmd_Type;
	if(Cmd_Type == 0x01)
	{
			frame[2] = 0x0D;	
		/*获取96位，12字节的ID号*/
		for( j = 0 ; j < 3 ; j++ )
			for( i = 0 ; i < 4 ; i++)
			{
				frame[8+i+j*4] = *((uint32_t*)(0x1FFFF7E8+j*4))>>(8 * (3-i));
			}
			data_length = 20;
	}
	else if(Cmd_Type == 0x02)
	{
		frame[2] = 0x06;	
		frame[8]	= MUX_option;
		data_length = 9;
	}
	/*获取校验值，放在帧尾处*/
	frame[data_length] = Checksum_Check(frame,data_length);
	Usart_SendArray(DEBUG_USARTx,frame,data_length+1);
}

/******************************************************
* Brief     : 计算激活码
* Parameter :	buffer:存储激活码的buffer
* Return    : 0:USB boot激活
*							1:USB 不激活
*******************************************************/
uint8_t Compare_Active_Code()
{
	uint8_t i = 0;
	uint32_t  Active_Code[2] ={0};
	uint32_t  Internal_buffer[2] = {0};
	Active_Code[0] = *((uint32_t*)0x1FFFF7E8)^*((uint32_t*)0x1FFFF7EC);	
	Active_Code[1] = *((uint32_t*)0x1FFFF7EC)^*((uint32_t*)0x1FFFF7f0);
	Internal_Flash_Read(ACTIVE,Internal_buffer,2);
	for( i = 0 ; i < 2 ; i++ )
	{
		if(Active_Code[i] != Internal_buffer[i])
		{
			return 1;
		}
	}
	return 0;
}
/******************************************************
* Brief     : 返回值处理
* Parameter :	err:错误参数
* Return   	: None
*******************************************************/
void Store_Active_Code(uint8_t *data_buf)
{
	uint8_t i = 0;
	uint8_t j = 0;
	uint32_t temp = 0;
	uint32_t buffer[8];
	for( i = 8 ;i < 16 ; i+=4 )
	{
		temp = data_buf[i];
		temp |= data_buf[i+1]<<8;
		temp |= data_buf[i+2]<<16;
		temp |= data_buf[i+3]<<24;
		buffer[j] = temp;
		j++;
	}
	Internal_Flash_Write(ACTIVE,buffer,2);
}
