#ifndef _USER_APP_H
#define _USER_APP_H
#include "stm32f10x.h"
#include "bsp_usart.h"
#include "usb_bsp.h"
#include "usb_hcd_int.h"
#include "usbh_core.h"
#include "usbh_usr.h" 
#include "ff.h"
#include "rw_flash.h"
#include "key.h"
#include "string.h"
#include "stdlib.h"

#define ACK 				0xff				//正常应答
#define NACK_NG 		0xf0				//校验失败
#define NACK_NS			0xf3				//不支持
#define NACK_BUSY	  0xfc 				//忙

#define  MAX_STORAGE    120*1024
typedef enum 
{
	FAIL = 0, 
  PASS = !FAIL
} Check_Status;

//typedef enum 
//{
//	FILE_OK			= 0x00,
//	MOUNT_FAIL 	= 0x01,
//	STAT_FAIL  	= 0x02,
//	OPEN_FAIL 	= 0x03,
//	READ_FAIL		= 0x04,
//	OVERPAGE		= 0x05,
//	UPDATE			= 0x06
//}FILE_Status;


void SoftReset(void);
uint8_t Checksum_Check(uint8_t *Data_buf,uint8_t Data_Size);
void fWDG_init(void);
uint8_t APP_Updata(char * File_Path);
uint8_t Detect_File(char *file_path);
void Unique_ID(uint8_t *ID);
void MCU_To_Host(uint8_t Cmd_Type,uint8_t MUX_option);
uint8_t Check_USB_Bin(char *file_path);
uint8_t Compare_Active_Code(void);
void Store_Active_Code(uint8_t *data_buf);
#endif

