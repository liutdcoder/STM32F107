/*************************************************************************
* Copyright (c) 2020, TD TECHNOLOGY
* All rights reserved.
*
* File name    :   main.c
* Brief        :   USB Updata app
*               Introduce the main function or content of this document briefly.
* Revision     :   1.0.1
* Author       :   liujiaxing 
* Date         :   2020.6.3
* Update       :   添加了一些注释
*************************************************************************/

#include "stm32f10x.h"
#include "user_app.h"
#include "basic_timer.h"

/*USBz指针，不能删*/
USB_OTG_CORE_HANDLE   USB_OTG_Core;
USBH_HOST             USB_Host;


/*串口接受数据标志*/
extern uint16_t RX_Status;
/*串口接受缓存*/
extern uint8_t RX_BUF[RX_LEN];
/*一帧数据的数据位长度*/
extern uint8_t RX_Data_Length;

/*IAP请求升级数据*/
uint32_t IAP_CMD[3]= {0x32};
uint32_t Jump_account = 0;
uint8_t flag = 0;
/**
  * @brief  主函数
  * @param  无
  * @retval 无
  */
/*最大长度*/

int main(void)
{	

  /*硬件外设初始化*/
  USART_Config();				//初始化USART 配置模式为 115200 8-N-1，中断接收
	KEY_Init();						//按键初始化
	TIMER_Init();					//定时器初始化
//	fWDG_init();					//看门狗初始化
	
	/*打印一些必要信息*/
	printf("TangDu Technology\r\n");								//打印公司名称
	printf("bootloader copyright 1.0.1\r\n");				//打印bootloader版本号
	printf("modify time:2020.6.1\r\n");						//bootloadr修改时间

	/*USB初始化*/
	USBH_Init(&USB_OTG_Core,USB_OTG_FS_CORE_ID,&USB_Host,&USBH_MSC_cb,&USR_Callbacks); 
	/*开始读不好，禁止读取内部代码*/	
	if(FLASH_GetReadOutProtectionStatus() != SET)
		FLASH_ReadOutProtection(ENABLE);
	
	while(1)
	{	
		/*USB处理函数，需放在主循环中，仅在枚举时起作用*/
		USBH_Process(&USB_OTG_Core, &USB_Host);
	
//		if(flag == 0x01)
//		{
//			
//		}
//			/*判断是否有激活码，有激活码直接跳转到app*/
//		if((Compare_Active_Code() == 0))
//		{
//			/*擦除激活码和IAP升级请求*/
//			Internal_Flash_Erase(ACTIVE,1);
//			
//			
//			/*跳转到app中*/
//			iap_load_app(SEC);
//		}

//		/*超过五秒无操作，跳转得到app中*/
		if(Jump_account >10)
		{
			//iap_load_app(SEC);
			if((Compare_Active_Code() == 0))
			{
				/*擦除激活码和IAP升级请求*/
				Internal_Flash_Erase(ACTIVE,1);
				/*跳转到app中*/
				iap_load_app(SEC);
			}
		else
		{
			SoftReset();
		}
		}
		/*接受完成一帧数据，开始处理*/
		if((RX_Status & 0x1000) != 0x00)
		{
			RX_Status = 0;
			/*进行数据帧校验比对*/
			if(Checksum_Check(RX_BUF,RX_Data_Length+3 )== RX_BUF[RX_Data_Length+3])
			{
				/*正确接受一帧数据，给主机发送响应*/
				Usart_SendByte(DEBUG_USARTx,ACK);
				switch(RX_BUF[7])
				{
					/*主机发送96位ID号命令,MCU返回ID号给主机*/
					case 0x01:
						//printf("96位ID号命令\r\n");
						MCU_To_Host(0x01,0x02);
					break;
					/*主机发送激活码命令，存储激活码到本地flash*/
					case 0x02:
						//printf("发送激活码命令\r\n");
						Store_Active_Code(RX_BUF);
					SoftReset();
					break;
					/*主机发送切换到IAP命令,MCU写入IAP数据到flash中，表示有IAP升级请求*/
					case 0x03:
						//printf("切换到IAP命令\r\n");
						Internal_Flash_Write(ACTIVE+8,IAP_CMD,1);
						SoftReset();
					break;
					default:
						break;
			    }
			}
			else
			{
				//printf("校验失败\r\n");
				/*校验失败，发送无响应给主机*/
				Usart_SendByte(DEBUG_USARTx,NACK_NG);
			}					
			RX_Data_Length = 0;
			/*清空接受缓存*/
			memset(RX_BUF,0,sizeof(RX_BUF));
		}	
		/*喂狗*/
	//	IWDG_ReloadCounter();
	}	
}
