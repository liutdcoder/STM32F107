#ifndef _KEY_H
#define _KEY_H
#include "stm32f10x.h"


#define KEYA_PORT_RCC					  RCC_APB2Periph_GPIOC
#define KEYA_PORT						   	GPIOC
#define KEYA_PORT_PIN					  GPIO_Pin_6

#define KEYB_PORT_RCC					  RCC_APB2Periph_GPIOC
#define KEYB_PORT						   	GPIOC
#define KEYB_PORT_PIN					  GPIO_Pin_7

#define KEYC_PORT_RCC					  RCC_APB2Periph_GPIOC
#define KEYC_PORT						   	GPIOC
#define KEYC_PORT_PIN					  GPIO_Pin_8

#define KEYD_PORT_RCC						RCC_APB2Periph_GPIOC
#define KEYD_PORT						   	GPIOC
#define KEYD_PORT_PIN						GPIO_Pin_9

void KEY_Init(void);
uint8_t CHECK_Key(uint8_t Key_Num);
uint8_t KEY_Value_Scan(GPIO_TypeDef *KEYX,uint16_t GPIO_PIN);
uint8_t CHECK_Key(uint8_t Key_Num);
uint8_t KEY_Value_Scan(GPIO_TypeDef *KEYX,uint16_t GPIO_PIN);
#endif
