/**
  ******************************************************************************
  * @file    usbh_usr.c
  * @author  MCD Application Team
  * @version V2.2.1
  * @date    17-March-2018
  * @brief   This file includes the usb host library user callbacks
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2015 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                      <http://www.st.com/SLA0044>
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------ */
#include <string.h>
#include "usbh_usr.h"
#include "user_app.h"
#include "ff.h"                 /* FATFS */
#include "usbh_msc_core.h"
#include "usbh_msc_scsi.h"
#include "usbh_msc_bot.h"

/** @addtogroup USBH_USER
* @{
*/

/** @addtogroup USBH_MSC_DEMO_USER_CALLBACKS
* @{
*/

/** @defgroup USBH_USR 
* @brief    This file includes the usb host stack user callbacks
* @{
*/

/** @defgroup USBH_USR_Private_TypesDefinitions
* @{
*/
/**
* @}
*/



extern USB_OTG_CORE_HANDLE USB_OTG_Core;
uint8_t USB_Stand_By_Flag = 0;
/**
* @}
*/
/* Points to the DEVICE_PROP structure of current device */
/* The purpose of this register is to speed up the execution */

USBH_Usr_cb_TypeDef USR_Callbacks = {
  USBH_USR_Init,
  USBH_USR_DeInit,
  USBH_USR_DeviceAttached,
  USBH_USR_ResetDevice,
  USBH_USR_DeviceDisconnected,
  USBH_USR_OverCurrentDetected,
  USBH_USR_DeviceSpeedDetected,
  USBH_USR_Device_DescAvailable,
  USBH_USR_DeviceAddressAssigned,
  USBH_USR_Configuration_DescAvailable,
  USBH_USR_Manufacturer_String,
  USBH_USR_Product_String,
  USBH_USR_SerialNum_String,
  USBH_USR_EnumerationDone,
  USBH_USR_UserInput,
  USBH_USR_MSC_Application,
  USBH_USR_DeviceNotSupported,
  USBH_USR_UnrecoveredError
};
/**
* @}
*/
/** @defgroup USBH_USR_Private_Constants
* @{
*/
/*--------------- LCD Messages ---------------*/
uint8_t MSG_HOST_INIT[] = "> Host Library Initialized\n";
uint8_t MSG_DEV_ATTACHED[] = "> Device Attached \n";
uint8_t MSG_DEV_DISCONNECTED[] = "> Device Disconnected\n";
uint8_t MSG_DEV_ENUMERATED[] = "> Enumeration completed \n";
uint8_t MSG_DEV_HIGHSPEED[] = "> High speed device detected\n";
uint8_t MSG_DEV_FULLSPEED[] = "> Full speed device detected\n";
uint8_t MSG_DEV_LOWSPEED[] = "> Low speed device detected\n";
uint8_t MSG_DEV_ERROR[] = "> Device fault \n";

uint8_t MSG_MSC_CLASS[] = "> Mass storage device connected\n";
uint8_t MSG_HID_CLASS[] = "> HID device connected\n";
uint8_t MSG_DISK_SIZE[] = "> Size of the disk in MBytes: \n";
uint8_t MSG_LUN[] = "> LUN Available in the device:\n";
uint8_t MSG_ROOT_CONT[] = "> Exploring disk flash ...\n";
uint8_t MSG_WR_PROTECT[] = "> The disk is write protected\n";
uint8_t MSG_UNREC_ERROR[] = "> UNRECOVERED ERROR STATE\n";

/*
* @}
*/


/**
* @}
*/

/** @defgroup USBH_USR_Private_Functions
* @{
*/

/**
* @brief  USBH_USR_Init 
*         Displays the message on LCD for host lib initialization
* @param  None
* @retval None
*/

void USBH_USR_Init(void)
{
//	printf("USB OTG FS MSC Host\r\n");
//	printf("> USB Host library started.\r\n");
//	printf("  USB Host Library v2.1.0\r\n\r\n");
}

/**
* @brief  USBH_USR_DeviceAttached 
*         Displays the message on LCD on device attached
* @param  None
* @retval None
*/

void USBH_USR_DeviceAttached(void)
{
  //printf("检测到USB设备插入!\r\n");
}
/**
* @brief  USBH_USR_UnrecoveredError
* @param  None
* @retval None
*/
void USBH_USR_UnrecoveredError(void)
{
	//printf("无法恢复的错误!!!\r\n\r\n");	
}


/**
* @brief  USBH_DisconnectEvent
*         Device disconnect event
* @param  None
* @retval Status
*/
void USBH_USR_DeviceDisconnected(void)
{

  //printf("USB设备拔出!\r\n");

}

/**
* @brief  USBH_USR_ResetUSBDevice 
* @param  None
* @retval None
*/
void USBH_USR_ResetDevice(void)
{
 // printf("复位设备...\r\n");
	
}


/**
* @brief  USBH_USR_DeviceSpeedDetected 
*         Displays the message on LCD for device speed
* @param  Device speed
* @retval None
*/
void USBH_USR_DeviceSpeedDetected(uint8_t DeviceSpeed)
{
 if(DeviceSpeed==HPRT0_PRTSPD_HIGH_SPEED)
	{
	//printf("高速(HS)USB设备!\r\n");
 	}  
	else if(DeviceSpeed==HPRT0_PRTSPD_FULL_SPEED)
	{
	//	printf("全速(FS)USB设备!\r\n"); 
	}
	else if(DeviceSpeed==HPRT0_PRTSPD_LOW_SPEED)
	{
//		printf("低速(LS)USB设备!\r\n");  
	}
	else
	{
	//	printf("设备错误!\r\n");  
	}
}

/**
* @brief  USBH_USR_Device_DescAvailable 
*         Displays the message on LCD for device descriptor
* @param  device descriptor
* @retval None
*/
void USBH_USR_Device_DescAvailable(void *DeviceDesc)
{
  USBH_DevDesc_TypeDef *hs;
	hs=DeviceDesc;   
//	printf("VID: %04Xh\r\n" , (uint32_t)(*hs).idVendor); 
//	printf("PID: %04Xh\r\n" , (uint32_t)(*hs).idProduct); 
}

/**
* @brief  USBH_USR_DeviceAddressAssigned 
*         USB device is successfully assigned the Address 
* @param  None
* @retval None
*/
void USBH_USR_DeviceAddressAssigned(void)
{
	//printf("从机地址分配成功!\r\n"); 
}


/**
* @brief  USBH_USR_Conf_Desc 
*         Displays the message on LCD for configuration descriptor
* @param  Configuration descriptor
* @retval None
*/
void USBH_USR_Configuration_DescAvailable(USBH_CfgDesc_TypeDef * cfgDesc,
                                          USBH_InterfaceDesc_TypeDef * itfDesc,
                                          USBH_EpDesc_TypeDef * epDesc)
{
  USBH_InterfaceDesc_TypeDef *id; 
	id = itfDesc;   
	if((*id).bInterfaceClass==0x08)
	{
		//printf("可移动存储器设备!\r\n"); 
	}else if((*id).bInterfaceClass==0x03)
	{
		//printf("HID 设备!\r\n"); 
	}    
}

/**
* @brief  USBH_USR_Manufacturer_String 
*         Displays the message on LCD for Manufacturer String 
* @param  Manufacturer String 
* @retval None
*/
void USBH_USR_Manufacturer_String(void *ManufacturerString)
{
	//printf("Manufacturer: %s\r\n",(char *)ManufacturerString);
}

/**
* @brief  USBH_USR_Product_String 
*         Displays the message on LCD for Product String
* @param  Product String
* @retval None
*/
void USBH_USR_Product_String(void *ProductString)
{
//	printf("Product: %s\r\n",(char *)ProductString);  
}

/**
* @brief  USBH_USR_SerialNum_String 
*         Displays the message on LCD for SerialNum_String 
* @param  SerialNum_String 
* @retval None
*/
void USBH_USR_SerialNum_String(void *SerialNumString)
{
	//printf("Serial Number: %s\r\n",(char *)SerialNumString);    
}



/**
* @brief  EnumerationDone 
*         User response request is displayed to ask application jump to class
* @param  None
* @retval None
*/
void USBH_USR_EnumerationDone(void)
{
 	//printf("设备枚举完成!\r\n\r\n");  
	
}


/**
* @brief  USBH_USR_DeviceNotSupported
*         Device is not supported
* @param  None
* @retval None
*/
void USBH_USR_DeviceNotSupported(void)
{
  printf("No registered class for this device. \n\r");
}


/**
* @brief  USBH_USR_UserInput
*         User Action for application state entry
* @param  None
* @retval USBH_USR_Status : User response for key button
*/
USBH_USR_Status USBH_USR_UserInput(void)
{
  printf("跳过用户确认步骤!\r\n");
	 	USB_Stand_By_Flag = 0x01;
	return USBH_USR_RESP_OK;
}

/**
* @brief  USBH_USR_OverCurrentDetected
*         Over Current Detected on VBUS
* @param  None
* @retval Status
*/
void USBH_USR_OverCurrentDetected(void)
{
  	printf("端口电流过大!!!\r\n");
}


/**
* @brief  USBH_USR_MSC_Application 
*         Demo application for mass storage
* @param  None
* @retval Status
*/

int USBH_USR_MSC_Application(void)
{	
	uint8_t Status = 0;
	uint8_t updata_Status = 0;
	uint32_t code_flash[2] = {0x00};
	uint32_t Internal_data[6];
	if(USB_Stand_By_Flag == 0x01)
	{
		/**************自动升级部分*****************/
		/*判断是否存在td_force.bin文件，并判断该bin文件
		与内部flahs是否一致，不一致则启动升级程序*/
		Status = Check_USB_Bin("0:td_force.bin");
	//	printf("Status = %d\r\n",Status);
		/*bin文件与内部app一致，无需更新*/
		if(Status == 0)
		{
		//	printf("无可用更新");	
		}
		if(Status == 0x05)
		{
				MCU_To_Host(0x02,0x01);
				//printf("开始升级\r\n");
				updata_Status = APP_Updata("0:td_force.bin");
			if(updata_Status == 0x00)
			{
				/*发送升级完成*/
				MCU_To_Host(0x02,0x02);
			}
			else if(updata_Status == 0x05)
			{
				/*发送文件过大*/
				MCU_To_Host(0x02,0x05);
			}
			else if(updata_Status == 0x06)
			{
				/*发送升级失败*/
				MCU_To_Host(0x02,0x03);
			}
		}	
		/**************手动升级部分*****************/
		/*查看是否有IAP升级请求，如果有，启动升级程序，写如td_updat.bin文件*/
		Internal_Flash_Read(ACTIVE,Internal_data,4);
		if(Internal_data[3] == 0x32)
		{
			Internal_Flash_Read(ACTIVE,code_flash,3);
			Internal_Flash_Erase(ACTIVE,1);
			Internal_Flash_Write(ACTIVE,code_flash,3);
			/*开始升级*/
			MCU_To_Host(0x02,0x01);
			updata_Status = APP_Updata("0:td_updat.bin");	
			if(updata_Status == 0x00)
			{
				/*发送升级完成*/
				MCU_To_Host(0x02,0x02);
			}
			else if(updata_Status == 0x05)
			{
				/*发送文件过大*/
				MCU_To_Host(0x02,0x05);
			}
			else if(updata_Status == 0x06)
			{
				/*发送升级失败*/
				MCU_To_Host(0x02,0x03);
			}
		}
	}
	USB_Stand_By_Flag = 0x00;	
	return 0;
}

/**
* @brief  Toggle_Leds
*         Toggle leds to shows user input state
* @param  None
* @retval None
*/

/**
* @brief  USBH_USR_DeInit
*         Deinit User state and associated variables
* @param  None
* @retval None
*/
void USBH_USR_DeInit(void)
{
 printf("重新初始化!!!\r\n");
	USB_Stand_By_Flag =0x00;
}


///************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
