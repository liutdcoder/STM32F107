#include "basic_timer.h"
static void NVIC_Configuration(void);
void TIMER_Init()
{
	TIM_TimeBaseInitTypeDef TIM_InitStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6,ENABLE);
	TIM_InitStructure.TIM_Prescaler = 36000-1;
	TIM_InitStructure.TIM_Period = 500;
	
	TIM_TimeBaseInit(TIM6, &TIM_InitStructure);
	
	// 清除计数器中断标志位
	TIM_ClearFlag(TIM6, TIM_FLAG_Update);

 // 开启计数器中断
	TIM_ITConfig(TIM6,TIM_IT_Update,ENABLE);
 
 // 使能计数器
	TIM_Cmd(TIM6, ENABLE);
	
	NVIC_Configuration();
}

static void NVIC_Configuration()
{
	NVIC_InitTypeDef NVIC_InitStructure;
// 设置中断组为 0
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	
	NVIC_InitStructure.NVIC_IRQChannel = TIM6_IRQn ;
	
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
// 设置抢占优先级为 3
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	
	NVIC_Init(&NVIC_InitStructure);
	
}