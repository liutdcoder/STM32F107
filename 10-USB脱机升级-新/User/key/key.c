#include "key.h"
static void Delay_ms( __IO uint32_t ms);
void KEY_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd( KEYA_PORT_RCC | KEYB_PORT_RCC | KEYC_PORT_RCC, ENABLE); 		
			 
  GPIO_InitStructure.GPIO_Pin = KEYA_PORT_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; ; 
  GPIO_Init(KEYA_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = KEYB_PORT_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; ; 
  GPIO_Init(KEYB_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = KEYC_PORT_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; ; 
  GPIO_Init(KEYC_PORT, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = KEYD_PORT_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; ; 
  GPIO_Init(KEYD_PORT, &GPIO_InitStructure);
}

uint8_t CHECK_Key(uint8_t Key_Num)
{
	unsigned char record = 0;
	switch(Key_Num)
	{
		case 0:
			if(KEY_Value_Scan(KEYA_PORT,KEYA_PORT_PIN) == 1)
			{
				record = 0x01;
			}
			break;
		case 1:
			if(KEY_Value_Scan(KEYB_PORT,KEYB_PORT_PIN) == 1)
			{
				record = 0x02;
			}
			break;
		case 2:
			if(KEY_Value_Scan(KEYC_PORT,KEYC_PORT_PIN) == 1)
			{
				record = 0x03;
			}
			break;
		case 3:
			if(KEY_Value_Scan(KEYD_PORT,KEYD_PORT_PIN) == 1)
			{
				record = 0x04;
			}
			break;
		default:
			break;
	}
	return record;
}

uint8_t KEY_Value_Scan(GPIO_TypeDef *KEYX,uint16_t GPIO_PIN)
{
	/*读取键值*/
	if(GPIO_ReadInputDataBit(KEYX,GPIO_PIN) == 0)
	{/*读取键值*/
		if(GPIO_ReadInputDataBit(KEYX,GPIO_PIN) == 0)
		{
			Delay_ms(300);
			return 1;
		}	
	}
	return 0;
}
static void Delay_ms( __IO uint32_t ms)
{
	uint32_t i;	
	SysTick_Config(SystemCoreClock/1000);
	
	for(i=0;i<ms;i++)
	{
		// 当计数器的值减小到0的时候，CRTL寄存器的位16会置1
		// 当置1时，读取该位会清0
		while( !((SysTick->CTRL)&(1<<16)) );
	}
	// 关闭SysTick定时器
	SysTick->CTRL &=~ SysTick_CTRL_ENABLE_Msk;
}
