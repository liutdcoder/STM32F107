/**
  ******************************************************************************
  * @file    usb_bsp.h
  * @author  MCD Application Team
  * @version V2.2.1
  * @date    17-March-2018
  * @brief   Specific api's relative to the used hardware platform
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2015 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                      <http://www.st.com/SLA0044>
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USB_BSP__H__
#define __USB_BSP__H__

/* Includes ------------------------------------------------------------------*/
#include "usb_core.h"

/** @addtogroup USB_OTG_DRIVER
  * @{
  */
  
/** @defgroup USB_BSP
  * @brief This file is the 
  * @{
  */ 


/** @defgroup USB_BSP_Exported_Defines
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup USB_BSP_Exported_Types
  * @{
  */ 
/**
  * @}
  */ 


/** @defgroup USB_BSP_Exported_Macros
  * @{
  */ 
/**
  * @}
  */ 

/** @defgroup USB_BSP_Exported_Variables
  * @{
  */ 
/**
  * @}
  */ 

#define HOST_POWERSW_PORT_RCC              RCC_APB2Periph_GPIOC
#define HOST_POWERSW_PORT                  GPIOC
#define HOST_POWERSW_VBUS                  GPIO_Pin_4

#define HOST_SOF_OUTPUT_RCC                RCC_APB2Periph_GPIOA
#define HOST_SOF_PORT                      GPIOA
#define HOST_SOF_SIGNAL                    GPIO_Pin_8

#define KEYA_PORT_RCC					  RCC_APB2Periph_GPIOC
#define KEYA_PORT						   	GPIOC
#define KEYA_PORT_PIN					  GPIO_Pin_6

#define KEYB_PORT_RCC					  RCC_APB2Periph_GPIOC
#define KEYB_PORT						   	GPIOC
#define KEYB_PORT_PIN					  GPIO_Pin_7

#define KEYC_PORT_RCC					  RCC_APB2Periph_GPIOC
#define KEYC_PORT						   	GPIOC
#define KEYC_PORT_PIN					  GPIO_Pin_8

#define KEYD_PORT_RCC						RCC_APB2Periph_GPIOC
#define KEYD_PORT						   	GPIOC
#define KEYD_PORT_PIN						GPIO_Pin_9
/** @defgroup USBH_USR_Exported_Defines
  * @{
  */ 
/* State Machine for the USBH_USR_ApplicationState */
#define USH_USR_FS_INIT       0
#define USH_USR_FS_READLIST   1
#define USH_USR_FS_WRITEFILE  2
#define USH_USR_FS_READFILE   3
#define USH_USR_FS_NULL       4
/** @defgroup USB_BSP_Exported_FunctionsPrototype
  * @{
  */ 
void BSP_Init(void);

void USB_OTG_BSP_Init (USB_OTG_CORE_HANDLE *pdev);
void USB_OTG_BSP_uDelay (const uint32_t usec);
void USB_OTG_BSP_mDelay (const uint32_t msec);
void USB_OTG_BSP_EnableInterrupt (USB_OTG_CORE_HANDLE *pdev);
void USB_OTG_BSP_TimerIRQ (void);
#ifdef USE_HOST_MODE
void USB_OTG_BSP_ConfigVBUS(USB_OTG_CORE_HANDLE *pdev);
void USB_OTG_BSP_DriveVBUS(USB_OTG_CORE_HANDLE *pdev,uint8_t state);
void USB_OTG_BSP_Resume(USB_OTG_CORE_HANDLE *pdev) ;                                                                
void USB_OTG_BSP_Suspend(USB_OTG_CORE_HANDLE *pdev);
void Delay_ms( __IO uint32_t ms);
void Delay_us( __IO uint32_t ms);
uint8_t CHECK_Key(uint8_t Key_Num);
uint8_t KEY_Value_Scan(GPIO_TypeDef *KEYX,uint16_t GPIO_PIN);
#endif /* USE_HOST_MODE */
/**
  * @}
  */ 

#endif /* __USB_BSP__H__ */

/**
  * @}
  */ 

/**
  * @}
  */ 
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

