/**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   串口中断接收测试
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火 F103-霸道 STM32 开发板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */ 
 
 
#include "stm32f10x.h"
#include "bsp_usart.h"
#include "usb_bsp.h"
#include "usb_hcd_int.h"
#include "usbh_core.h"
#include "usbh_usr.h" 
#include "ff.h"

USB_OTG_CORE_HANDLE   USB_OTG_Core;
USBH_HOST             USB_Host;

FRESULT result;
FATFS fs;
FIL file;
DIR  dir;
FILINFO *info;
FILINFO fno;
//递归扫描文件时使用的路径
char scan_path[255] = "0:";

UINT fnum;            					  /* 文件成功读写数量 */
BYTE ReadBuffer[1024]={0};        /* 读缓冲区 */
BYTE WriteBuffer[1024] =              /* 写缓冲区*/
"欢迎使用野火STM32 F429开发板 今天是个好日子，新建文件系统测试文件\r\n";  

/**
  * @brief  主函数
  * @param  无
  * @retval 无
  */
static void read_write_flie(void);
int main(void)
{	
  /*初始化USART 配置模式为 115200 8-N-1，中断接收*/
  USART_Config();
	/* 发送一个字符串 */
	Usart_SendString( DEBUG_USARTx,"这是一个串口中断接收回显实验\n");
	printf("欢迎使用野火STM32开发板\n\n\n\n");
	USBH_Init(&USB_OTG_Core,USB_OTG_FS_CORE_ID,&USB_Host,&USBH_MSC_cb,&USR_Callbacks);  

  while(1)
	{	
				USBH_Process(&USB_OTG_Core, &USB_Host);
		if(CHECK_Key(0) == 0x01)
		{
			 result = f_mount(&fs,"0:",1);	/* Mount a logical drive */
			 if(result != FR_OK)
      {
        printf("\r\n文件系统挂载失败！\r\n");
        continue; 
      }
			else
			{
				printf("文件系统挂载成功\r\n");
			}
			
			 result = f_mount(&fs,"0:",1);	/* Mount a logical drive */
			 if(result != FR_OK)
      {
        printf("\r\n文件系统取消失败！\r\n");
        continue; 
      }
			else
			{
				printf("文件系统取消挂载成功\r\n");
			}
		}
		if(CHECK_Key(1) == 0x02)
		{
			result = f_mount(&fs,"0:",1);	/* Mount a logical drive */
			 if(result != FR_OK)
      {
        printf("\r\n文件系统挂载失败！\r\n");
        continue; 
      }
			else
			{
				printf("文件系统挂载成功\r\n");
			}
			read_write_flie();
			result = f_mount(&fs,"0:",1);	/* Mount a logical drive */
			 if(result != FR_OK)
      {
        printf("\r\n文件系统取消失败！\r\n");
        continue; 
      }
			else
			{
				printf("文件系统取消挂载成功\r\n");
			}
		}
		if(CHECK_Key(2) == 0x03)
		{
			result = f_open(&file,"0:2.txt",FA_WRITE|FA_READ|FA_OPEN_ALWAYS);
			if(result == 0)
			{
				printf("打开成功\r\n");
				result = f_write(&file,WriteBuffer,sizeof(WriteBuffer),&fnum);
				if(result == 0)
					printf("文件写入成功\r\n");
				else
					printf("文件写入失败\r\n");
				f_close(&file);
			}
			else
			{
				printf("打开失败\r\n");
			}	
		}
	}	
}
static void read_write_flie(void)
{	

///*----------------------- 文件系统测试：写测试 -----------------------------*/
//	/* 打开文件，如果文件不存在则创建它 */
//	printf("\r\n****** 即将进行文件写入测试... ******\r\n");	
//	result = f_open(&file, "0:FatFs.txt",FA_CREATE_ALWAYS | FA_WRITE );
//	if ( result == FR_OK )
//	{
//		printf("》打开/创建FatFs读写测试文件.txt文件成功，向文件写入数据。\r\n");
//    /* 将指定存储区内容写入到文件内 */
//		result=f_write(&file,WriteBuffer,sizeof(WriteBuffer),&fnum);
//    if(result==FR_OK)
//    {
//      printf("》文件写入成功，写入字节数据：%d\n",fnum);
//      printf("》向文件写入的数据为：\r\n%s\r\n",WriteBuffer);
//    }
//    else
//    {
//      printf("！！文件写入失败：(%d)\n",result);
//    }    
//		/* 不再读写，关闭文件 */
//    f_close(&file);
//	}
//	else
//	{	
//		printf("！！打开/创建文件失败。\r\n");
//	}
//	
/*------------------- 文件系统测试：读测试 ------------------------------------*/

	uint32_t i = 0;
	uint32_t j = 0;
	uint16_t buffer_count = 0;
	uint16_t buffer_rest = 0;
	printf("****** 即将进行文件读取测试... ******\r\n");
	result = f_stat("0:USART.bin", &fno);
	if(result == FR_OK)
	{
				printf("Size: %u\n", fno.fsize);
        printf("Timestamp: %u/%02u/%02u, %02u:%02u\n",
               (fno.fdate >> 9) + 1980, fno.fdate >> 5 & 15, fno.fdate & 31,
               fno.ftime >> 11, fno.ftime >> 5 & 63);

	}
	
	buffer_count = fno.fsize /1024 ;
	buffer_rest = fno.fsize%1024;
	printf("buffer_count = %d\r\n",buffer_count);
	result = f_open(&file, "0:USART.bin", FA_OPEN_EXISTING | FA_READ); 	 
	if(result == FR_OK)
	{	
		printf("》打开文件成功。\r\n");
		for( j = 0 ;j < buffer_count ; j++)
		{	
			result = f_read(&file, ReadBuffer,sizeof(ReadBuffer), &fnum); 
			if(result==FR_OK)
			{
				printf("》文件读取成功,读到字节数据：%d\r\n",fnum);
     // printf("》读取得的文件数据为：\r\n%s \r\n", ReadBuffer);	
				for( i = 0; i < fnum ; i++)
				printf(" 0x%x ", ReadBuffer[i]);			
			}
			else
			{
				printf("！！文件读取失败：(%d)\n",result);
			}			
		}		
			result = f_read(&file, ReadBuffer,buffer_rest, &fnum); 
			if(result==FR_OK)
			{
				printf("》文件读取成功,读到字节数据：%d\r\n",fnum);
     // printf("》读取得的文件数据为：\r\n%s \r\n", ReadBuffer);	
				for( i = 0; i < fnum ; i++)
				printf(" 0x%x ", ReadBuffer[i]);			
			}
			else
			{
				printf("！！文件读取失败：(%d)\n",result);
			}
	}
	else
	{
		printf("！！打开文件失败。\r\n");
	}
	/* 不再读写，关闭文件 */
	f_close(&file);	
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

/*********************************************END OF FILE**********************/
