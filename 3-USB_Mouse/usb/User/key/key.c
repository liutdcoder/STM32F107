#include "key.h"
#include "bsp_usart.h"
/**
  * Function Name : KEY_Init.
  * Description   : 按键初始化
  * Input         : Keys: keys received from terminal.
  * Output        : None.
  * Return value  : None.
  */
void KEY_Init()
{
	RCC_APB2PeriphClockCmd(KEY1_CLOCK|KEY2_CLOCK,ENABLE);
	GPIO_InitTypeDef GPIO_InitStructure;
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin = KEY1_PIN;
	GPIO_Init(KEY1_PORT,&GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Pin = KEY2_PIN;
	GPIO_Init(KEY2_PORT,&GPIO_InitStructure);
}
/**
  * Function Name : Scan_Key_Value.
  * Description   : 获取键值
  * Input         : Keys: keys received from terminal.
  * Output        : None.
	* Return value  :0 NO_Value 1:key1 2 :key2 
  */
uint8_t Scan_Key_Value(void)
{
	if(KEY1_VALUE == KEY_PRESS)
	{
		SysTick_Delay_Ms(100);
		if(KEY1_VALUE == KEY_PRESS)
			return 1;
	}
	if(KEY2_VALUE == KEY_PRESS)
	{
		SysTick_Delay_Ms(100);
		if(KEY2_VALUE == KEY_PRESS)
			return 2;
	}
	return 0;
}

void SysTick_Delay_Ms( __IO uint32_t ms)
{
	uint32_t i;	
	SysTick_Config(SystemCoreClock/1000);
	
	for(i=0;i<ms;i++)
	{
		// 当计数器的值减小到0的时候，CRTL寄存器的位16会置1
		// 当置1时，读取该位会清0
		while( !((SysTick->CTRL)&(1<<16)) );
	}
	// 关闭SysTick定时器
	SysTick->CTRL &=~ SysTick_CTRL_ENABLE_Msk;
}

void SysTick_Delay_Us( __IO uint32_t us)
{
	uint32_t i;	
	SysTick_Config(SystemCoreClock/1000000);
	
	for(i=0;i<us;i++)
	{
		// 当计数器的值减小到0的时候，CRTL寄存器的位16会置1
		// 当置1时，读取该位会清0
		while( !((SysTick->CTRL)&(1<<16)) );
	}
	// 关闭SysTick定时器
	SysTick->CTRL &=~ SysTick_CTRL_ENABLE_Msk;
}
