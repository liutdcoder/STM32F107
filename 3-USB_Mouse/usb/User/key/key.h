#ifndef _KEY_H
#define _KEY_H
#include "stm32f10x.h"
#define KEY1_PORT 			GPIOC
#define KEY1_PIN				GPIO_Pin_5
#define KEY1_CLOCK			RCC_APB2Periph_GPIOC

#define KEY2_PORT 			GPIOE
#define KEY2_PIN				GPIO_Pin_3
#define KEY2_CLOCK			RCC_APB2Periph_GPIOE

#define KEY1_VALUE 			GPIO_ReadInputDataBit(KEY1_PORT,KEY1_PIN)
#define KEY2_VALUE 			GPIO_ReadInputDataBit(KEY2_PORT,KEY2_PIN)
#define KEY_PRESS       0

void KEY_Init(void);
uint8_t Scan_Key_Value(void);
void SysTick_Delay_Us( __IO uint32_t us);
void SysTick_Delay_Ms( __IO uint32_t ms);
#endif
