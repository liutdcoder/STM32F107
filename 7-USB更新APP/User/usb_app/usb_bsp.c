/**
  ******************************************************************************
  * @file    usb_bsp.c
  * @author  MCD Application Team
  * @version V2.2.1
  * @date    17-March-2018
  * @brief   This file implements the board support package for the USB host library
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2015 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                      <http://www.st.com/SLA0044>
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------ */

#include "usb_bsp.h"

/** @addtogroup USBH_USER
* @{
*/

/** @defgroup USB_BSP
  * @brief This file is responsible to offer board support package
  * @{
  */

/** @defgroup USB_BSP_Private_Defines
  * @{
  */
#define USE_ACCURATE_TIME
#define TIM_MSEC_DELAY                     0x01
#define TIM_USEC_DELAY                     0x02
#define HOST_OVRCURR_PORT                  GPIOE
#define HOST_OVRCURR_LINE                  GPIO_Pin_1
#define HOST_OVRCURR_PORT_SOURCE           GPIO_PortSourceGPIOE
#define HOST_OVRCURR_PIN_SOURCE            GPIO_PinSource1
#define HOST_OVRCURR_PORT_RCC              RCC_APB2Periph_GPIOE
#define HOST_OVRCURR_EXTI_LINE             EXTI_Line1
#define HOST_OVRCURR_IRQn                  EXTI1_IRQn

#ifdef USE_STM3210C_EVAL
#define HOST_POWERSW_PORT_RCC             RCC_APB2Periph_GPIOC
#define HOST_POWERSW_PORT                 GPIOC
#define HOST_POWERSW_VBUS                 GPIO_Pin_9
#endif

#if defined(USE_STM324x9I_EVAL)
#ifdef USE_USB_OTG_FS
#define HOST_POWERSW_FS1_VBUS                    IO16_Pin_7
#endif

#ifdef USE_USB_OTG_HS
#define HOST_POWERSW_FS2_VBUS                    IO16_Pin_9
#endif
#endif

#if defined(USE_STM322xG_EVAL) || defined(USE_STM324xG_EVAL)
#ifdef USE_USB_OTG_FS
#define HOST_POWERSW_PORT_RCC            RCC_AHB1Periph_GPIOH
#define HOST_POWERSW_PORT                GPIOH
#define HOST_POWERSW_VBUS                GPIO_Pin_5
#endif                          /* USE_USB_OTG_FS */
#endif                          /* defined(USE_STM322xG_EVAL) ||
                                 * defined(USE_STM324xG_EVAL) */

#define HOST_SOF_OUTPUT_RCC                RCC_APB2Periph_GPIOA
#define HOST_SOF_PORT                      GPIOA
#define HOST_SOF_SIGNAL                    GPIO_Pin_8

/**
  * @}
  */


/** @defgroup USB_BSP_Private_TypesDefinitions
  * @{
  */
/**
  * @}
  */



/** @defgroup USB_BSP_Private_Macros
  * @{
  */
/**
  * @}
  */

/** @defgroup USBH_BSP_Private_Variables
  * @{
  */
ErrorStatus HSEStartUpStatus;
#ifdef USE_ACCURATE_TIME
__IO uint32_t BSP_delay = 0;
#endif
/**
  * @}
  */

/** @defgroup USBH_BSP_Private_FunctionPrototypes
  * @{
  */

#ifdef USE_ACCURATE_TIME
//static void BSP_SetTime(uint8_t Unit);
//static void BSP_Delay(uint32_t nTime, uint8_t Unit);
//static void USB_OTG_BSP_TimeInit(void);
#endif
/**
  * @}
  */

/** @defgroup USB_BSP_Private_Functions
  * @{
  */

/**
  * @brief  USB_OTG_BSP_Init
  *         Initializes BSP configurations
  * @param  None
  * @retval None
  */
void USB_OTG_BSP_Init(USB_OTG_CORE_HANDLE *pdev)
{
    GPIO_InitTypeDef GPIO_InitStructure;
  
  /* Select USBCLK source */
  RCC_OTGFSCLKConfig(RCC_OTGFSCLKSource_PLLVCO_Div3);

  /* Enable the USB clock */ 
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_OTG_FS, ENABLE) ;

  RCC_APB2PeriphClockCmd( KEYA_PORT_RCC | KEYB_PORT_RCC | KEYC_PORT_RCC, ENABLE); 		
			 
  GPIO_InitStructure.GPIO_Pin = KEYA_PORT_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; ; 
  GPIO_Init(KEYA_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = KEYB_PORT_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; ; 
  GPIO_Init(KEYB_PORT, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin = KEYC_PORT_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; ; 
  GPIO_Init(KEYC_PORT, &GPIO_InitStructure);
	
	 GPIO_InitStructure.GPIO_Pin = KEYD_PORT_PIN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; ; 
  GPIO_Init(KEYD_PORT, &GPIO_InitStructure);
}
/**
  * @brief  USB_OTG_BSP_EnableInterrupt
  *         Configures USB Global interrupt
  * @param  None
  * @retval None
  */
//USB OTG 中断设置,开启USB FS中断
//pdev:USB OTG内核结构体指针
void USB_OTG_BSP_EnableInterrupt(USB_OTG_CORE_HANDLE *pdev)
{
  NVIC_InitTypeDef NVIC_InitStructure;
  /* Enable USB Interrupt */
  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

  NVIC_InitStructure.NVIC_IRQChannel = OTG_FS_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	
}
//USB OTG 中断设置,开启USB FS中断
//pdev:USB OTG内核结构体指针
void USB_OTG_BSP_DisableInterrupt(void)
{ 
}
/**
  * @brief  BSP_Drive_VBUS
  *         Drives the Vbus signal through IO
  * @param  state : VBUS states
  * @retval None
  */

void USB_OTG_BSP_DriveVBUS(USB_OTG_CORE_HANDLE * pdev, uint8_t state)
{
}

/**
  * @brief  USB_OTG_BSP_ConfigVBUS
  *         Configures the IO for the Vbus and OverCurrent
  * @param  None
  * @retval None
  */

void USB_OTG_BSP_ConfigVBUS(USB_OTG_CORE_HANDLE * pdev)
{
}
	
uint8_t KEY_Value_Scan(GPIO_TypeDef *KEYX,uint16_t GPIO_PIN)
{
	/*读取键值*/
	if(GPIO_ReadInputDataBit(KEYX,GPIO_PIN) == 0)
	{/*读取键值*/
		if(GPIO_ReadInputDataBit(KEYX,GPIO_PIN) == 0)
		{
			Delay_ms(300);
			return 1;
		}	
	}
	return 0;
}

uint8_t CHECK_Key(uint8_t Key_Num)
{
	unsigned char record = 0;
	switch(Key_Num)
	{
		case 0:
			if(KEY_Value_Scan(KEYA_PORT,KEYA_PORT_PIN) == 1)
			{
				record = 0x01;
			}
			break;
		case 1:
			if(KEY_Value_Scan(KEYB_PORT,KEYB_PORT_PIN) == 1)
			{
				record = 0x02;
			}
			break;
		case 2:
			if(KEY_Value_Scan(KEYC_PORT,KEYC_PORT_PIN) == 1)
			{
				record = 0x03;
			}
			break;
		case 3:
			if(KEY_Value_Scan(KEYD_PORT,KEYD_PORT_PIN) == 1)
			{
				record = 0x04;
			}
			break;
		default:
			break;
	}
	return record;
}
/**
  * @brief  USB_OTG_BSP_TimeInit
  *         Initializes delay unit using Timer2
  * @param  None
  * @retval None
  */
//static void USB_OTG_BSP_TimeInit(void)
//{
//#ifdef USE_ACCURATE_TIME
//  NVIC_InitTypeDef NVIC_InitStructure;

//  /* Set the Vector Table base address at 0x08000000 */
//  NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x00);

//  /* Configure the Priority Group to 2 bits */
//  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

//  /* Enable the TIM2 global Interrupt */
//  NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;

//  NVIC_Init(&NVIC_InitStructure);

//  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
//#endif
//}
void Delay_us( __IO uint32_t ms)
{
	uint32_t i;	
	SysTick_Config(SystemCoreClock/1000000);
	
	for(i=0;i<ms;i++)
	{
		// 当计数器的值减小到0的时候，CRTL寄存器的位16会置1
		// 当置1时，读取该位会清0
		while( !((SysTick->CTRL)&(1<<16)) );
	}
	// 关闭SysTick定时器
	SysTick->CTRL &=~ SysTick_CTRL_ENABLE_Msk;
}

void Delay_ms( __IO uint32_t ms)
{
	uint32_t i;	
	SysTick_Config(SystemCoreClock/1000);
	
	for(i=0;i<ms;i++)
	{
		// 当计数器的值减小到0的时候，CRTL寄存器的位16会置1
		// 当置1时，读取该位会清0
		while( !((SysTick->CTRL)&(1<<16)) );
	}
	// 关闭SysTick定时器
	SysTick->CTRL &=~ SysTick_CTRL_ENABLE_Msk;
}
//USB_OTG us级延时函数
//本例程采用SYSTEM文件夹的delay.c里面的delay_us函数实现
//官方例程采用的是定时器2来实现的.
//usec:要延时的us数.
void USB_OTG_BSP_uDelay (const uint32_t usec)
{ 
   	Delay_us(usec);
}
//USB_OTG ms级延时函数
//本例程采用SYSTEM文件夹的delay.c里面的delay_ms函数实现
//官方例程采用的是定时器2来实现的.
//msec:要延时的ms数.
void USB_OTG_BSP_mDelay (const uint32_t msec)
{  
	Delay_ms(msec);
}
   


///**
//  * @brief  USB_OTG_BSP_uDelay
//  *         This function provides delay time in micro sec
//  * @param  usec : Value of delay required in micro sec
//  * @retval None
//  */
//void USB_OTG_BSP_uDelay(const uint32_t usec)
//{

//#ifdef USE_ACCURATE_TIME
//  BSP_Delay(usec, TIM_USEC_DELAY);
//#else
//  __IO uint32_t count = 0;
//  const uint32_t utime = (120 * usec / 7);
//  do
//  {
//    if (++count > utime)
//    {
//      return;
//    }
//  }
//  while (1);
//#endif

//}


///**
//  * @brief  USB_OTG_BSP_mDelay
//  *          This function provides delay time in milli sec
//  * @param  msec : Value of delay required in milli sec
//  * @retval None
//  */
//void USB_OTG_BSP_mDelay(const uint32_t msec)
//{
//#ifdef USE_ACCURATE_TIME
//  BSP_Delay(msec, TIM_MSEC_DELAY);
//#else
//  USB_OTG_BSP_uDelay(msec * 1000);
//#endif

//}


///**
//  * @brief  USB_OTG_BSP_TimerIRQ
//  *         Time base IRQ
//  * @param  None
//  * @retval None
//  */

//void USB_OTG_BSP_TimerIRQ(void)
//{
//#ifdef USE_ACCURATE_TIME

//  if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
//  {
//    TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
//    if (BSP_delay > 0x00)
//    {
//      BSP_delay--;
//    }
//    else
//    {
//      TIM_Cmd(TIM2, DISABLE);
//    }
//  }
//#endif
//}

//#ifdef USE_ACCURATE_TIME
///**
//  * @brief  BSP_Delay
//  *         Delay routine based on TIM2
//  * @param  nTime : Delay Time 
//  * @param  unit : Delay Time unit : mili sec / micro sec
//  * @retval None
//  */
//static void BSP_Delay(uint32_t nTime, uint8_t unit)
//{

//  BSP_delay = nTime;
//  BSP_SetTime(unit);
//  while (BSP_delay != 0);
//  TIM_Cmd(TIM2, DISABLE);
//}

///**
//  * @brief  BSP_SetTime
//  *         Configures TIM2 for delay routine based on TIM2
//  * @param  unit : msec /usec
//  * @retval None
//  */
//static void BSP_SetTime(uint8_t unit)
//{
//  TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

//  TIM_Cmd(TIM2, DISABLE);
//  TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);


//  if (unit == TIM_USEC_DELAY)
//  {
//    TIM_TimeBaseStructure.TIM_Period = 11;
//  }
//  else if (unit == TIM_MSEC_DELAY)
//  {
//    TIM_TimeBaseStructure.TIM_Period = 11999;
//  }
//  TIM_TimeBaseStructure.TIM_Prescaler = 5;
//  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
//  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

//  TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
//  TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

//  TIM_ARRPreloadConfig(TIM2, ENABLE);

//  /* TIM IT enable */
//  TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

//  /* TIM2 enable counter */
//  TIM_Cmd(TIM2, ENABLE);
//}

//#endif

///**
//* @}
//*/

///**
//* @}
//*/

///**
//* @}
//*/

///************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
