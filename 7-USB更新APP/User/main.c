/**
  ******************************************************************************
  * @file    main.c
  * @author  fire
  * @version V1.0
  * @date    2013-xx-xx
  * @brief   串口中断接收测试
  ******************************************************************************
  * @attention
  *
  * 实验平台:野火 F103-霸道 STM32 开发板 
  * 论坛    :http://www.firebbs.cn
  * 淘宝    :https://fire-stm32.taobao.com
  *
  ******************************************************************************
  */ 
 
 
#include "stm32f10x.h"
#include "bsp_usart.h"
#include "usb_bsp.h"
#include "usb_hcd_int.h"
#include "usbh_core.h"
#include "usbh_usr.h" 
#include "ff.h"
#include "rw_flash.h"
USB_OTG_CORE_HANDLE   USB_OTG_Core;
USBH_HOST             USB_Host;

FRESULT result;
FATFS fs;
FIL file;
DIR  dir;
FILINFO *info;
FILINFO fno;
//递归扫描文件时使用的路径
char scan_path[255] = "0:";

UINT fnum;            					  /* 文件成功读写数量 */
BYTE ReadBuffer[2048]={0};        /* 读缓冲区 */
BYTE WriteBuffer[2048] =              /* 写缓冲区*/
"欢迎使用野火STM32 F429开发板 今天是个好日子，新建文件系统测试文件\r\n";  
uint32_t app_buf[512] = {0};
/**
  * @brief  主函数
  * @param  无
  * @retval 无
  */

static void read_write_flie(void);
int main(void)
{	
  /*初始化USART 配置模式为 115200 8-N-1，中断接收*/
  USART_Config();
	/* 发送一个字符串 */
	Usart_SendString( DEBUG_USARTx,"这是一个串口中断接收回显实验\n");
	printf("欢迎使用野火STM32开发板\n\n\n\n");
	USBH_Init(&USB_OTG_Core,USB_OTG_FS_CORE_ID,&USB_Host,&USBH_MSC_cb,&USR_Callbacks);  
  while(1)
	{	
		USBH_Process(&USB_OTG_Core, &USB_Host);
		if(CHECK_Key(1) == 0x02)
		{
			result = f_mount(&fs,"0:",1);	/* Mount a logical drive */
			 if(result != FR_OK)
      {
        printf("\r\n文件系统挂载失败！\r\n");
        continue; 
      }
			else
			{
				printf("文件系统挂载成功\r\n");
			}
			read_write_flie();
			result = f_mount(&fs,"0:",1);	/* Mount a logical drive */
			 if(result != FR_OK)
      {
        printf("\r\n文件系统取消失败！\r\n");
        continue; 
      }
			else
			{
				printf("文件系统取消挂载成功\r\n");
			}
		}
	}	
}

static void read_write_flie(void)
{	
	uint32_t i = 0;
	uint32_t j = 0;
	uint32_t rev_byte = 0;
	uint16_t buffer_count = 0;
	uint16_t buffer_rest = 0;
	
	uint16_t Sect_word = 0;
	
	
	printf("****** 即将进行文件读取测试... ******\r\n");
	result = f_stat("0:USART.bin", &fno);
	
	/*输出文件信息*/
	if(result == FR_OK)
	{
				printf("Size: %u\n", fno.fsize);
        printf("Timestamp: %u/%02u/%02u, %02u:%02u\n",
               (fno.fdate >> 9) + 1980, fno.fdate >> 5 & 15, fno.fdate & 31,
               fno.ftime >> 11, fno.ftime >> 5 & 63);
	}
	buffer_count = fno.fsize /2048 ;						//扇区数
	buffer_rest = fno.fsize%2048;								//不足一个扇区剩下的字节数目
	/*打开bin文件*/
	result = f_open(&file, "0:USART.bin", FA_OPEN_EXISTING | FA_READ); 	 
	if(result == FR_OK)
	{	
		printf("》打开文件成功。\r\n");
		for( j = 0 ;j < buffer_count ; j++)
		{	
			result = f_read(&file, ReadBuffer,sizeof(ReadBuffer), &fnum); 
			if(result==FR_OK)
			{
				printf("》文件读取成功,读到字节数据：%d\r\n",fnum);
   
				for( i = 0; i < fnum ; i++)
					printf(" 0x%x ", ReadBuffer[i]);			
			}
			else
			{
				printf("！！文件读取失败：(%d)\n",result);
			}	
			IAP_Write_Appbin(ReadBuffer,SEC+j * FLASH_PAGE_SIZE,2048);
		}		
			result = f_read(&file, ReadBuffer,buffer_rest, &fnum); 
			IAP_Write_Appbin(ReadBuffer,SEC+buffer_count* FLASH_PAGE_SIZE,buffer_rest);
		
			if(result==FR_OK)
			{
				printf("》文件读取成功,读到字节数据：%d\r\n",fnum);
				for( i = 0; i < fnum ; i++)
				printf(" 0x%x ", ReadBuffer[i]);			
			}
			else
			{
				printf("！！文件读取失败：(%d)\n",result);
			}
	}
	else
	{
		printf("！！打开文件失败。\r\n");
	}
	/* 不再读写，关闭文件 */
	f_close(&file);	
	iap_load_app(SEC);
}
