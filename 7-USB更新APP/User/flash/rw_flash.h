#ifndef _RW_FLASH_H
#define _RW_FLASH_H
#include "stm32f10x.h"
typedef  void (*iapfun)(void);				//定义一个函数类型的参数.   
//FLASH起始地址
#define STM32_FLASH_BASE 0x08000000 	//STM32 FLASH的起始地址
//////////////////////////////////////////////////////////////////////////////////////////////////////
//用户根据自己的需要设置
#define STM32_FLASH_SIZE 256 	 		//所选STM32的FLASH容量大小(单位为K)
#define STM32_FLASH_WREN 1              //使能FLASH写入(0，不是能;1，使能)
//////////////////////////////////////////////////////////////////////////////////////////////////////

/* STM32大容量产品每页大小2KByte，中、小容量产品每页大小1KByte */
#if defined (STM32F10X_HD) || defined (STM32F10X_HD_VL) || defined (STM32F10X_CL) || defined (STM32F10X_XL)
  #define FLASH_PAGE_SIZE    ((uint16_t)0x800)	//2048
#else
  #define FLASH_PAGE_SIZE    ((uint16_t)0x400)	//1024
#endif

typedef enum 
{
	FAILED = 0, 
  PASSED = !FAILED
} TestStatus;

#define SEC   0x08010000 
int Erase_Write_Flash(uint32_t WriteAddr,uint32_t *pBuff,uint32_t NumToWrite);
void Internal_Flash_Write(uint32_t WriteAddr, uint32_t *pBuff,uint32_t NumToWrite);
void Internal_Flash_Read(uint32_t ReadAddr, uint32_t *pBuff,uint32_t NumToRead);
void Internal_Flash_Erase(uint32_t Erase_addr,uint8_t NumToErase);
void iap_load_app(u32 appxaddr);
uint8_t IAP_Write_Appbin(uint8_t *Read_buf,uint32_t Write_addr,uint32_t NumToWrite);
#endif
