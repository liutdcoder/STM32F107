#include "rw_flash.h"
iapfun jump2app; 
//设置栈顶地址
//addr:栈顶地址
__asm void MSR_MSP(u32 addr) 
{
	MSR MSP, r0 			//set Main Stack value
	BX r14
}

/******************************************************
* Brief     : 擦除内部flash写入数据并校验
* Parameter : 
*           WriteAddr:要写入的起始地址
*						pBuff		 :要写入的数据缓存指针(单位为字)
*						NumToWrite:要写入的字数
* Return    : MemoryProgramStatus：PASSED(校验通过)
*																	 FAILED(校验失败)		
*******************************************************/
int Erase_Write_Flash(uint32_t WriteAddr,uint32_t *pBuff,uint32_t NumToWrite)
{
	uint32_t EraseCounter = 0x00; 	//记录要擦除多少页
	uint32_t Address = 0x00;				//记录写入的地址
	uint32_t NbrOfPage = 0x00;			//记录写入多少页
	uint32_t WRITE_END_ADDR = 0;
	FLASH_Status FLASHStatus = FLASH_COMPLETE; //记录每次擦除的结果	
	TestStatus MemoryProgramStatus = PASSED;//记录整个测试结果
  /* 解锁 */
  FLASH_Unlock();
	
  /* 计算要擦除多少页 */
  NbrOfPage = (NumToWrite *4 / FLASH_PAGE_SIZE) +1;

  /* 清空所有标志位 */
  FLASH_ClearFlag(FLASH_FLAG_EOP | FLASH_FLAG_PGERR | FLASH_FLAG_WRPRTERR);	

	WRITE_END_ADDR = (NumToWrite*4 + WriteAddr);
  /* 按页擦除*/
  for(EraseCounter = 0; (EraseCounter < NbrOfPage) && (FLASHStatus == FLASH_COMPLETE); EraseCounter++)
  {
    FLASHStatus = FLASH_ErasePage(WriteAddr + (FLASH_PAGE_SIZE * EraseCounter));
	}
  /* 向内部FLASH写入数据 */
  Address = WriteAddr;
  while((Address < WRITE_END_ADDR) && (FLASHStatus == FLASH_COMPLETE))
  {
    FLASHStatus = FLASH_ProgramWord(Address, *pBuff);
    Address = Address + 4;
		pBuff++;
  }
	
  FLASH_Lock();
  /* 检查写入的数据是否正确 */
  Address = WriteAddr;
	pBuff -=NumToWrite;
 
  while((Address < WRITE_END_ADDR)&& (MemoryProgramStatus != FAILED))
  {
    if((*(__IO uint32_t*) Address) != *pBuff)
    {
      MemoryProgramStatus = FAILED;
    }
    Address += 4;	
		pBuff++;
  }
	return MemoryProgramStatus;
}

/******************************************************
* Brief     : 读写内部flash
* Parameter : 
*           WriteAddr:要写入的起始地址
*						pBuff		 :要写入的数据缓存指针
*						NumToWrite:要写入的字节数(以字为单位，一个扇区写512个字，大容量mcu一个扇区是2K)
* Return    : None.
*******************************************************/
void Internal_Flash_Write(uint32_t WriteAddr,uint32_t *pBuff,uint32_t NumToWrite)
{
		uint32_t Sector_num = 0;					//扇区地址
		uint32_t Sector_addr_off = 0;			//扇区内偏移地址
		uint32_t off_addr = 0;						//去除基地址的偏移量
		uint32_t  Address = WriteAddr;
		uint16_t i = 0;
		/*判断是否为非法地址*/
		if((WriteAddr<STM32_FLASH_BASE)||(WriteAddr>=(STM32_FLASH_BASE+1024*STM32_FLASH_SIZE)))
			return;//非法地址
		FLASH_Unlock();	
		off_addr = WriteAddr - STM32_FLASH_BASE;							//实际偏移地址
		Sector_num = off_addr/FLASH_PAGE_SIZE;								//扇区地址
		Sector_addr_off = (off_addr%FLASH_PAGE_SIZE)/2;				//最后一个扇区空间
//		for(i = 0; i < (Sector_num + 1); i ++)
//		{
//			FLASH_ErasePage( WriteAddr + i * FLASH_PAGE_SIZE);
//		}
		for(i = 0; i< NumToWrite ; i++)
		{
			FLASH_ProgramWord(Address,*pBuff);
			Address += 4;
			pBuff++;
		}
		FLASH_Lock();
}
/******************************************************
* Brief     : 读内部flash
* Parameter : 
*           WriteAddr:要读取的起始地址
*						pBuff		 :要读入的数据缓存指针
*						NumToWrite:要读入的字节数
* Return    : None.
*******************************************************/
void Internal_Flash_Erase(uint32_t Erase_addr,uint8_t NumToErase)
{
		uint16_t i = 0;
		FLASH_Unlock();
		for( i = 0; i < NumToErase; i++)
		{
				FLASH_ErasePage(Erase_addr + i*FLASH_PAGE_SIZE);
		}
		FLASH_Lock();
}
/******************************************************
* Brief     : 读内部flash
* Parameter : 
*           WriteAddr:要读取的起始地址
*						pBuff		 :要读入的数据缓存指针
*						NumToWrite:要读入的字节数
* Return    : None.
*******************************************************/
void Internal_Flash_Read(uint32_t ReadAddr, uint32_t *pBuff,uint32_t NumToRead)
{
	uint16_t i = 0;
	uint32_t  ADDR = ReadAddr;
	for( i = 0;i < NumToRead; i++)
	{
		*pBuff = *((uint32_t *)ADDR);
		pBuff++;
		ADDR+=4;
	}
}
/******************************************************
* Brief     : 跳转到指定地址，并开始运行
* Parameter : 
*           appxaddr:跳转的目的地址
* Return    : None.
*******************************************************/
void iap_load_app(u32 appxaddr)
{
	if(((*(vu32*)appxaddr)&0x2FFE0000)==0x20000000)	//检查栈顶地址是否合法.
	{ 
		jump2app=(iapfun)*(vu32*)(appxaddr+4);		//用户代码区第二个字为程序开始地址(复位地址)		
		MSR_MSP(*(vu32*)appxaddr);					//初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
		jump2app();									//跳转到APP.
	}
}		 
u32 iapbuf[512]; 	//2K字节缓存 
/******************************************************
* Brief     : 写入对应APP程序的bin文件
* Parameter : appbuf:所要写入的数据指针
*           	appxaddr:跳转的目的地址
*							appsize:所要写入的字节数目
* Return    : None.
*******************************************************/
uint8_t IAP_Write_Appbin(uint8_t *appbuf,u32 appxaddr,u32 appsize)
{
	u32 t;
	uint8_t Write_Status;
	u16 i=0;
	u32 temp;
	u32 fwaddr=appxaddr;//当前写入的地址
	u8 *dfu=appbuf;	
	for(t=0;t<appsize;t+=4)
	{						   
		temp=(u32)dfu[3]<<24;   
		temp|=(u32)dfu[2]<<16;    
		temp|=(u32)dfu[1]<<8;
		temp|=(u32)dfu[0];	  
		dfu+=4;//偏移4个字节
		iapbuf[i++]=temp;	    
		if(i==512)
		{
			i=0; 
			Write_Status = Erase_Write_Flash(fwaddr,iapbuf,512);
			if(Write_Status != PASSED)
			{
				return 1;
			}
			fwaddr+=2048;//偏移2048  512*4=2048
		}
	} 
	if(i)
		Erase_Write_Flash(fwaddr,iapbuf,i);//将最后的一些内容字节写进去.  
}
